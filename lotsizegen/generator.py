# lotsizegen is a instance generator for lot sizing problems.
# Copyright (C) 2017 Julien JPK <julienjpk@email.com>

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.

# You should have received a copy of the GNU Affero General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import numpy.random as npr


def check(ni, nt, demand, loss, tcap, cap):
    """
    Checks an instance's feasibility based on demand and capacity values.
    :param ni: The number of products.
    :param nt: The number of periods.
    :param demand: The demand matrix.
    :param loss: The setup loss matrix.
    :param tcap: The period-oriented capacity vector.
    :param cap: The products/period capacity matrix.
    :return: True if the solution checks out, False otherwise.
    """
    for i in range(ni):
        if any(demand[i][t] + loss[i][t] > cap[i][t] for t in range(nt)) or \
                any(demand[i][t] > tcap[t] for t in range(nt)):
            return False

    for t in range(nt):
        for i in range(ni):
            c_dem = sum(demand[i][tt] + loss[i][t] for tt in range(t + 1))
            c_cap = sum(cap[i][tt] for tt in range(t + 1))
            c_tcap = sum(tcap[tt] for tt in range(t + 1))

            if c_dem > c_cap or c_dem > c_tcap:
                return False

    return True


def generate(ni: int, nt: int):
    """
    Generates and prints out a random instance.
    :param ni: The number of products.
    :param nt: The number of periods.
    """
    tcap = cap = loss = prod = setup = holding = demand = None
    feasible = False

    while not feasible:
        demand = [[npr.random_integers(0, 100) for _ in range(nt)]
                  for _ in range(ni)]
        loss = [[1] * nt] * ni
        prod = [[1] * nt] * ni
        holding = [[round(npr.uniform(0.2, 1.2), 2) for _ in range(nt)]
                   for _ in range(ni)]

        mean_dem = [float(np.mean([demand[i][t] + loss[i][t]
                                   for i in range(ni)])) for t in range(nt)]
        tcap_base = [ni * mean_dem[t] for t in range(nt)]
        tcap = [npr.random_integers(tcap_base[t], np.round(1.4 * tcap_base[t]))
                for t in range(nt)]

        cap = [tcap] * ni
        setup = [[npr.choice((10, 50, 100, 200, 500)) for _ in range(nt)]
                 for _ in range(ni)]

        feasible = check(ni, nt, demand, loss, tcap, cap)

    print(("%d\n%d" + "\n\n%s" * 7) % (
        ni, nt,
        ' '.join(str(t) for t in tcap),
        '\n'.join(' '.join(str(i) for i in t) for t in cap),
        '\n'.join(' '.join(str(i) for i in t) for t in loss),
        '\n'.join(' '.join(str(i) for i in t) for t in prod),
        '\n'.join(' '.join(str(i) for i in t) for t in setup),
        '\n'.join(' '.join(str(i) for i in t) for t in holding),
        '\n'.join(' '.join(str(i) for i in t) for t in demand)
    ))
