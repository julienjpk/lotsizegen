#!/usr/bin/env python3

# lotsizegen is a instance generator for lot sizing problems.
# Copyright (C) 2017 Julien JPK <julienjpk@email.com>

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.

# You should have received a copy of the GNU Affero General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

from lotsizegen.generator import generate
import argparse as ap


def main():
    parser = ap.ArgumentParser()
    parser.add_argument("ni", type=int, help="The number of products.")
    parser.add_argument("nt", type=int, help="The number of periods.")

    args = parser.parse_args()
    generate(args.ni, args.nt)

if __name__ == '__main__':
    main()
