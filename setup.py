#!/usr/bin/env python3

# lotsizegen is a instance generator for lot sizing problems.
# Copyright (C) 2017 Julien JPK <julienjpk@email.com>

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.

# You should have received a copy of the GNU Affero General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup, find_packages

setup(
    name='lotsizegen',
    version='1.0.0.dev1',
    description='An instance generator for lot-sizing problems.',
    url='https://github.com/julienjpk/lotsizegen',
    author='Julien JPK',
    author_email='julienjpk@email.com',
    license='AGPL-3.0',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Education',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU Affero General Public License v3 '
        'or later (AGPLv3+)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Scientific/Engineering',
        'Topic :: Scientific/Engineering :: Mathematics',
        'Topic :: Utilities'
    ],
    keywords='lot-sizing cmilsp instance generator',
    packages=find_packages(),
    install_requires=['numpy'],
    entry_points={
        'console_scripts': [
            'lotsizegen = lotsizegen.__main__:main'
        ]
    }
)
