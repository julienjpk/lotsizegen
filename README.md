# lotsizegen : lot sizing instance generator #

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)

`lotsizegen` is a small Python 3 script which generates random lot sizing 
problem instances. 

**This work is the implementation of an algorithm presented in the following 
[reference][1] (Appendix A) :**

    Vahid Lotfi, Wun-Hwa Chen, An optimal algorithm for the multi-item 
    capacitated production planning problem, European Journal of Operational 
    Research, Volume 52, Issue 2, 1991, Pages 17 9-193, ISSN 0377-2217, 
    http://dx.doi.org/10.1016/0377-2217(91)90079-B.
    
## Installing ##

Pretty straightforward:

    $ sudo ./setup.py install
    $ lotsizegen <N> <T>
    
## Details ##

I tried to use an output format which would be convenient for C++ developers,
using spaces as separators and giving the dimensions first. The output consists
of several values, vectors, and matrices:

1. A number of products
2. A number of periods
3. Per-period capacities
4. Per-period, per-product capacities (copies from the above)
5. Per-period, per-product capacity loss factors (set at 1)
6. Per-period, per-product production costs (per unt, set at 1)
7. Per-period, per-product setup costs (per setup)
8. Per-period, per-product holding costs (per unit)
9. Per-period, per-product demand

Typically, this is enough for (most) CMILSP (*Capacited Multi-Item Lot Sizing 
Problems*) cases, although you should feel free to edit the code if you need to.

*Note:* at the moment, I don't need to include capacity loss or production costs
in my work, so the script uses dummy values (1) for that. I'll edit it later if 
I need to.

[1]: http://dx.doi.org/10.1016/0377-2217(91)90079-B
